1. Soal 1 Membuat Database

    create database myshop;

2. Soal 2 Membuat Table di Dalam Database
use myshop;
create table users(
    -> id int auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255),
    -> primary key(id)
    -> );

create table items(
    -> id int auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int,
    -> stock int,
    -> category_id int,
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

    create table categories(
    -> id int auto_increment,
    -> name varchar(255),
    -> primary key(id)
    -> );

3. Soal 3 Memasukkan Data pada Table
insert into users(name,email,password)
    -> values ("John Doe","john@doe.com","john123"),
    -> ("Jane Doe","jane@doe.com","jenita123");

    insert into categories(name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

    insert into items Values(null,"Sumsang b50","hape keren dari merek sumsang",4000000,100,1);
    insert into items Values(null,"Uniklooh","baju keren dari brand ternama",500000,50,2);
    insert into items Values(null,"IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

4.  Soal 4 Mengambil Data dari Database
    a. select id,name,email from users;

    b.  select * from items where price>= 1000000;
        select * from items where name like 'uniklo%';
    
    c. c. select items.name, items.description, items.price,
    ->  items.stock, items.category_id, categories.name AS kategori from items join categories on items.category_id = categories.id;

5. Soal 5 Mengubah Data dari Database
    UPDATE items SET price ='2500000' where name='Sumsang b50';